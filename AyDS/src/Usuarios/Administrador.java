package Usuarios;

public class Administrador extends Persona {//ADMINISTRADORES

	private String legajo;
	
	public Administrador (String nombre, String apellido, Integer dni, String legajo)
	{
		super(nombre, apellido, dni);
		this.setLegajo(legajo);
	}
	
	/* MIRAR CLASE PERSONA */
	@Override
	public void modifPersona() {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
	public String getLegajo() {
		return legajo;
	}
	public void setLegajo(String legajo) {
		this.legajo = legajo;
	}
	
	
}
