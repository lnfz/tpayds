package Usuarios;

public class Usuario extends Persona{ //PORTERO O DOCENTE

		private Integer telefono;
		private String cargo;
		private String domicilio;
		
		public Usuario (String nombre, String apellido, Integer dni, String cargo)
		{
			super(nombre, apellido, dni);
			this.setCargo(cargo);
			this.setTelefono(0);
			this.setDomicilio("");
		}
		
		/* MIRAR CLASE PERSONA */
		@Override
		public void modifPersona() {
			// TODO Auto-generated method stub
			
		}
		
		
		
		public Integer getTelefono() {
			return telefono;
		}
		public void setTelefono(Integer telefono) {
			this.telefono = telefono;
		}
		public String getCargo() {
			return cargo;
		}
		public void setCargo(String cargo) {
			this.cargo = cargo;
		}
		public String getDomicilio() {
			return domicilio;
		}
		public void setDomicilio(String domicilio) {
			this.domicilio = domicilio;
		}

}
