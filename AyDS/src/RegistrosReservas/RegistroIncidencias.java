package RegistrosReservas;

import java.util.Date;
import Usuarios.Administrador;


public class RegistroIncidencias {

	private Date fecha;
	private Administrador persona;
	private String descripcion;
	private String nota;
	
	
	/*QUIEN REALIZA EL REGISTRO? UNA CLASE QUE LA ADMINISTRE HACIENDO DE CONTROLADOR, LA CLASE ADMINISTRADOR O
	  LA PROPIA CLASE?*/
	public void modificarRegistro()
	{
		
	}
	
	
	
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Administrador getPersona() {
		return persona;
	}
	public void setPersona(Administrador persona) {
		this.persona = persona;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	

}
