package Vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Image;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import java.awt.ScrollPane;
import java.awt.Color;
import javax.swing.JLabel;

public class abmusuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsuarios;
	private JTable usuarios;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					abmusuario frame = new abmusuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public abmusuario() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtUsuarios = new JTextField();
		txtUsuarios.setBackground(Color.LIGHT_GRAY);
		txtUsuarios.setBounds(10, 29, 424, 25);
		txtUsuarios.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtUsuarios.setHorizontalAlignment(SwingConstants.CENTER);
		txtUsuarios.setText("Usuarios");
		contentPane.add(txtUsuarios);
		txtUsuarios.setColumns(10);
		
		JButton btnAgregarUs = new JButton("Agregar");
		btnAgregarUs.setBounds(10, 65, 77, 25);
		contentPane.add(btnAgregarUs);
		btnAgregarUs.setBackground(java.awt.Color.BLUE);
		
		JButton btnModificarUs = new JButton("Modificar");
		btnModificarUs.setBackground(Color.BLUE);
		btnModificarUs.setBounds(10, 122, 77, 23);
		contentPane.add(btnModificarUs);
		
		JButton btnQuitarUs = new JButton("Quitar");
		btnQuitarUs.setBackground(Color.BLUE);
		btnQuitarUs.setBounds(10, 174, 77, 23);
		contentPane.add(btnQuitarUs);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(97, 65, 327, 185);
		contentPane.add(scrollPane);
		
		usuarios = new JTable();
		usuarios.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
					"Nombre", "Apellido", "DNI", "Cargo", "Telefono", "Domicilio"
			}
		));
		scrollPane.setViewportView(usuarios);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setBackground(Color.BLUE);
		btnVolver.setBounds(10, 225, 77, 25);
		contentPane.add(btnVolver);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setBounds(10, 4, 32, 25);
		contentPane.add(lblNewLabel);
		/*lblNewLabel.setIcon(new ImageIcon(getClass().getResource("/Logo/descarga.jpg")));*/
		ImageIcon fot = new ImageIcon(getClass().getResource("/Logo/descarga.jpg"));
		Icon icono = new ImageIcon(fot.getImage().getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(), Image.SCALE_DEFAULT));
		lblNewLabel.setIcon(icono);
		
	}
}
