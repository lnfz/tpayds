package Aulas;

import java.util.ArrayList;

public class Aula {

	private String nombre;
	private String piso ;
	private String facultad;
	private String descripcion;
	private Horario hora;
	private ArrayList<String> estado; //array de string
	//ArrayList<Personaje> p1 = new ArrayList<Personaje> ();
	
	public Aula (String nombre, String piso, String facultad, String descripcion, Horario hora, ArrayList<String> estado)
	{
		this.setNombre(nombre);
		this.setPiso(piso);
		this.setFacultad(facultad);
		this.setDescripcion(descripcion);
		this.setHora(hora);
		this.setEstado(estado);
	}
	
	
	public void listarHorariosDisp() //AQUI O EN LA CLASE AdministradorAula?
	{
		
	}
	
	public void modificarAula()
	{
		
	}
	
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPiso() {
		return piso;
	}
	public void setPiso(String piso) {
		this.piso = piso;
	}
	public String getFacultad() {
		return facultad;
	}
	public void setFacultad(String facultad) {
		this.facultad = facultad;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Horario getHora() {
		return hora;
	}
	public void setHora(Horario hora) {
		this.hora = hora;
	}
	public ArrayList<String> getEstado() {
		return estado;
	}
	public void setEstado(ArrayList<String> estado) {
		this.estado = estado;
	}
	
	

}
